const express = require('express');
const router = express.Router();
const JsonStorage = require('../jsonStorage');
const UserRepository = require('../repositories/userRepository');
const userStorage = new JsonStorage('../data/users.json');
const userRepository = new UserRepository('./data/users.json');
const User = require('../model/user');
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
module.exports = {
    getAllUsers(req, res) {
        let jsonArray = userRepository.getUsers();
        let page = parseInt(req.query.page);
        let per_page = parseInt(req.query.per_page);
        let skipped_items = (page - 1) * per_page;
        let newJsonArray = [];
        var counter = 0;
        for (var i = 0 + skipped_items; i < per_page + skipped_items && i < jsonArray.length; i++) {
            newJsonArray[counter] = jsonArray[i];
            counter++;
        }
        res.json(newJsonArray);
    },
    getUserById(req, res) {
        let jsonArrayUser = userRepository.getUsers();
        const found = jsonArrayUser.some(user => user.id === parseInt(req.params.id));
        if (found) {
            res.json(jsonArrayUser[req.params.id - 1]);
        } else {
            res.status(400).json({ msg: `Wrong id ${req.params.id}` });
        }
    },
    addUser(req, res) {
        let a = new User(
            userRepository.nextId,
            req.body.login,
            req.body.fullname,
            req.body.role,
            req.body.registeredAt,
            req.body.avaURL,
            true
        );
        if (!a.login || !a.fullname) {
            return res.status(400).json({ msg: "something is not right..." });
        }
        userRepository.addUser(a);
        res.json(userRepository.getUsers());
    },
    updateUser(req, res) {
        let jsonArrayUser = userRepository.getUsers();
        const found = jsonArrayUser.some(user => user.id === parseInt(req.params.id));
        if (found) {
            let a = new User(
                parseInt(req.params.id),
                req.body.login,
                req.body.fullname,
                req.body.role,
                req.body.registeredAt,
                req.body.avaURL,
                true
            );
            jsonArrayUser.forEach(user => {
                if (user.id === parseInt(req.params.id)) {
                    userRepository.updateUser(a);
                    res.json({ msg: 'updated' });
                }
            })
        } else {
            res.status(400).json({ msg: `Wrong id ${req.params.id}` });
        }
    },
    deleteUser(req, res) {
        let jsonArrayUser = userRepository.getUsers();
        const found = jsonArrayUser.some(user => user.id === parseInt(req.params.id));
        if (found) {
            userRepository.deleteUser(req.params.id);
            res.json({ msg: 'deleted', users: userRepository.getUsers() });
        } else {
            res.status(400).json({ msg: `Wrong id ${req.params.id}` });
        }
    }
};
