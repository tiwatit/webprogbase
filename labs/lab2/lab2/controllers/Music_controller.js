const express = require('express');
const router = express.Router();
const JsonStorage = require('../jsonStorage');
const MusicRepository = require('../repositories/musicRepository');
const musicStorage = new JsonStorage('../data/music.json');
const musicRepository = new MusicRepository('./data/music.json');
const Muse = require('../model/music');
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
module.exports = {
    getAllSongs(req,res) {
        let jsonArray = musicRepository.getMusic();
        let page = parseInt(req.query.page);
        let per_page = parseInt(req.query.per_page);
        let skipped_items = (page - 1) * per_page;
        let newJsonArray = [];
        var counter = 0;
        for (var i = 0 + skipped_items; i < per_page + skipped_items && i < jsonArray.length; i++) {
            newJsonArray[counter] = jsonArray[i];
            counter++;
        }
        res.json(newJsonArray);
    },
    getSongById(req, res) {
        let jsonArrayMuse = musicRepository.getMusic();
        const found = jsonArrayMuse.some(Muse => Muse.id === parseInt(req.params.id));
        if (found) {
            res.json(jsonArrayMuse[req.params.id - 1]);
        } else {
            res.status(400).json({ msg: `Wrong id ${req.params.id}` });
        }
    },
    addSong(req, res) {
        let a = new Muse(
            musicRepository.nextId,
            req.body.name,
            req.body.duration,
            req.body.group
        );
        if (!a.name || !a.group) {
            return res.status(400).json({ msg: "something is not right..." });
        }
        musicRepository.addMusic(a);
        res.json(musicRepository.getMusic());
    },
    changeSong(req, res) {
        let jsonArrayMuse = musicRepository.getMusic();
        const found = jsonArrayMuse.some(Muse => Muse.id === parseInt(req.params.id));
        if (found) {
            let a = new Muse(
                parseInt(req.params.id), //work
                req.body.name,
                req.body.duration,
                req.body.group
            );
            jsonArrayMuse.forEach(Muse => {
                if (Muse.id === parseInt(req.params.id)) {
                    musicRepository.updateMuse(a);
                    res.json({ msg: 'updated' });
                }
            })
        } else {
            res.status(400).json({ msg: `Wrong id ${req.params.id}` });
        }
    },
    deleteSong(req, res) {
        let jsonArrayMuse = musicRepository.getMusic();
        const found = jsonArrayMuse.some(Muse => Muse.id === parseInt(req.params.id));
        if (found) {
            musicRepository.deleteMusic(req.params.id);
            res.json({ msg: 'deleted', Muses: musicRepository.getMusic() });
        } else {
            res.status(400).json({ msg: `Wrong id ${req.params.id}` });
        }
    }
}
