const express = require('express');
const router = express.Router();
/**
 * returns a specific music
 * @route GET /api/Music/{id}
 * @group Music - music operations
 * @param {integer} id.path.required - id of the Music - eg: 1
 * @returns {Music.model} 200 - Music object
 * @returns {Error} 404 - Music not found
 */
/**
 * Returns all Music
 * @route GET /api/Music
 * @group Music - music operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<Music>} Music - a page with music
 */

/**
 * creates a new Music
 * @route POST /api/Music
 * @group Music - music operations
 * @param {Music.model} id.body.required - new music object
 * @returns {Music.model} 201 - added User object
 */
/**
 * Edits A specific song
 * @route PUT /api/Music
 * @group Music - music operations
 * @param {integer} id.path.required - id of the Music - eg: 1
 * @param {Music.model} id.body.required - new music object
 * @returns {Music.model} 200 - changed music object
 */
/**
 * Deletes a specific song
 * @route DELETE /api/Music/{id}
 * @group Music - music operations
 * @param {integer} id.path.required - id of the Music - eg: 1
 * @returns {Music.model} 200 - deleted Music object
 * @returns {Error} 404 - Music not found
 */

const bodyParser = require('body-parser');
const as = require('../../controllers/Music_controller');
const { getAllSongs, getSongById, addSong, changeSong, deleteSong } = require('../../controllers/Music_controller');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router
    .get('/', getAllSongs)
    .get('/:id', getSongById)
    .post('/', addSong)
    .put('/:id', changeSong)
    .delete('/:id', deleteSong);

module.exports = router;