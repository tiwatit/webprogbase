const Media = require('./../model/media');
const jsonStorage = require('./../jsonStorage');
class MediaRepository extends jsonStorage {
    addMedia(item) {
        const items = this.readItems();
        const NewMedia = new Media(
            items['nextId'],
            item.path
        );
        this.incrementNextId();
        items['items'].push(NewMedia);
        this.writeItems(items);
        return NewMedia.id;
    }

    getMediaById(id) {
        const items = this.readItems();
        for (const item of items['items']) {
            if (item.id == id) {
                return new Media(item.id, item.path);
            }
        }
        return null;
    }
}
module.exports = MediaRepository;