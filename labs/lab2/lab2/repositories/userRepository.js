// repositories/user_repository.js
const User = require('./../model/user');
const jsonStorage = require('./../jsonStorage');
class UserRepository extends jsonStorage {
    getUsers() {
        const items = this.readItems();
        let userArray = [User];
        let k = 0;
        for (const item of items['items']) {
            userArray[k] = new User(
                item.id,
                item.login,
                item.fullname,
                item.role,
                item.registeredAt,
                item.avaUrl,
                item.isEnabled
            );
            k++;
        }
        return userArray;
    }

    getUserById(id) {
        const items = this.readItems();
        for (const item of items['items']) {
            if (item.id == id) {
                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
            }
        }
        return null;
    }

    addUser(user) {
        let items = this.readItems();
        user.id = this.nextId;
        this.incrementNextId();
        items['items'].push(user);
        this.writeItems(items);
        console.log("new id: " + user.id);
        return;
    }
    updateUser(NewUser) {
        let counter = 0;
        const items = this.readItems();
        for (let i = 0; i < items['items'].length; i++) {
            if (NewUser.id == items['items'][counter]['id']) {
                items['items'].splice(counter, 1, NewUser);
                this.writeItems(items);
                return true;
            }
            counter++;
        }
        return false;
    }

    deleteUser(id) {
        const items = this.readItems();
        let k = 0;
        for (const item of items['items']) {
            if (item.id == id) {
                items['items'].splice(k, 1);
                this.writeItems(items);
                return;
            }
            k++;
        }
    }
};


module.exports = UserRepository;
