// model/user.js
/**
 * typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname - some description here
 * @property {integer} role - 1=admin, 0=default
 * @property {string} registeredAt - registration date
 * @property {string} avaUrl - link to the avatar
 * @property {boolean} isEnabled - active?
 */



class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;   //number
        this.registeredAt = registeredAt;   //string
        this.avaUrl = avaUrl;   //string
        this.isEnabled = isEnabled; //boolean
    }
};

module.exports = User;