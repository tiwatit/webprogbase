const routes = require('./routes/api/User');
const routes2 = require('./routes/api/Music_comps');
const express = require('express');
const routes3 = require('./routes/api/Media')
const app = express();
const port = 3000;
const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);

const options = {
    swaggerDefinition: {
        info: {
            description: 'description for lab2',
            title: 'Lab2',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: ["application/json"],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './model/**/*.js']
};
expressSwagger(options);


app.use('/api/User',routes);
app.use('/api/User/:id', routes);
app.use('/api/Music', routes2);
app.use('/api/Music/:id', routes2);
app.use('/api/Media', routes3);
app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
