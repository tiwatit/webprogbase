// model/music.js
/**
 * @typedef Music
 * @property {integer} id
 * @property {string} name.required - name of the song
 * @property {string} duration - duration of the song
 * @property {string} group - group
 */

class Music {
    constructor(id,name,duration,group) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.group = group;
    }
};

module.exports = Music;