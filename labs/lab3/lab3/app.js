const routes = require('./routes/api/User');
const routes2 = require('./routes/api/Music_comps');
const express = require('express');
const routes3 = require('./routes/api/Media')
const morgan = require("morgan")
const path = require('path')
const mustache = require('mustache-express')
const consolidate = require('consolidate')
const busboyBodyParser = require('busboy-body-parser')
const multer = require('multer')
const app = express();
const port = 3000;
const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);


app.use(express.static('view'))
app.use(morgan('dev'))
const options = {
    swaggerDefinition: {
        info: {
            description: 'description for lab3',
            title: 'Lab3',
            version: '1.0.0',
        },
        host: 'localhost:3000',
        produces: ["application/json"],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './model/**/*.js']
};
expressSwagger(options);


app.get('/Music/new', function (req, res) {
    res.render('add');
});

app.use(busboyBodyParser());





app.set('views',path.join(__dirname, 'view'))
app.set('view engine', 'mst')
app.engine('mst', mustache());
app.get('/', (req, res) => res.render('index'))
app.get('/about', (req, res) => res.render('about'))
app.use('/User/', routes);
app.use('/User/:id', routes);
app.use('/Music/', routes2);
app.post('/Music/addMuse', routes2);
app.use('/Music/:id', routes2);
app.use('/Media', routes3);
app.listen(port, () => {
    console.log(`Server is running at http://localhost:${port}`);
});
