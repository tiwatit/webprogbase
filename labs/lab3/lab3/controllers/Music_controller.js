const express = require('express');
const router = express.Router();
const JsonStorage = require('../jsonStorage');
const MusicRepository = require('../repositories/musicRepository');
const musicStorage = new JsonStorage('../data/music.json');
const musicRepository = new MusicRepository('./data/music.json');
const Muse = require('../model/music');
const Media = require('../model/media');
const MediaRepository = require('../repositories/mediaRepository');
const mediaStorage = new MediaRepository('./data/media.json');
const bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: true }));
const multer = require('multer');
router.use(bodyParser.json());
module.exports = {
    getAllSongs(req, res) {
        let jsonArray = musicRepository.getMusic();
        console.log(jsonArray)
        let page = 1;//parseInt(req.query.page);
        let per_page = 100;//parseInt(req.query.per_page);
        let skipped_items = (page - 1) * per_page;
        let music = [];
        var counter = 0;
        for (var i = 0; i < jsonArray.length; i++) {
            var str = jsonArray[i].name;
            console.log(req.query);
            if (!str.includes(req.query.find)) {
                jsonArray.splice(i, 1);
                i--;
            }
        }
        for (var i = 0 + skipped_items; i < per_page + skipped_items && i < jsonArray.length; i++) {
            music[counter] = jsonArray[i];
            counter++;
        }
        console.log(music);
        res.render('music', { music });
    },
    getSongById(req, res) {
        let jsonArrayMuse = musicRepository.getMusicById(req.params.id);
        if (jsonArrayMuse != null) {
            console.log(jsonArrayMuse.group);
            muse = jsonArrayMuse;
            res.render('muse', { muse });
        }else {
            res.status(400).json({ msg: `Wrong id ${req.params.id}` });
        }
    },
    addSong(req, res) {
        console.log(req.query.name);
        let new_media = new Media(
            req.body.id,
            req.files
        )
        let a = new Muse(
            musicRepository.nextId,
            req.body.name,
            req.body.duration,
            req.body.group,
            mediaStorage.addMedia(new_media)
        );
        if (!a.name || !a.group) {
            return res.status(400).json({ msg: "something is not right..." });
        }
        musicRepository.addMusic(a);
        res.redirect("/Music/" + a.id);
    },
    changeSong(req, res) {
        let jsonArrayMuse = musicRepository.getMusic();
        const found = jsonArrayMuse.some(Muse => Muse.id === parseInt(req.params.id));
        if (found) {
            let a = new Muse(
                parseInt(req.params.id), //work
                req.body.name,
                req.body.duration,
                req.body.group
            );
            jsonArrayMuse.forEach(Muse => {
                if (Muse.id === parseInt(req.params.id)) {
                    musicRepository.updateMuse(a);
                    res.json({ msg: 'updated' });
                }
            })
        } else {
            res.status(400).json({ msg: `Wrong id ${req.params.id}` });
        }
    },
    deleteSong(req, res) {
        console.log("kek")
        let jsonArrayMuse = musicRepository.getMusicById(req.params.id);
        if (jsonArrayMuse != null) {
            console.log(jsonArrayMuse.group);
            muse = jsonArrayMuse;
            musicRepository.deleteMusic(muse.id);
            res.redirect("/Music/?page=1&per_page=2");
            musicRepository.nextId
        } else {
            res.status(400).json({ msg: `Wrong id delete ${req.params.id}` });
        }
    }
}
