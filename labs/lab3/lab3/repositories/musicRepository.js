const Music = require('./../model/music');
const jsonStorage = require('./../jsonStorage')
class MusicRepository extends jsonStorage {
    getMusic() {
        const items = this.readItems();
        let musicArray = [Music];
        let k = 0;
        for (const item of items['items']) {
            musicArray[k] = new Music(
                item.id,
                item.name,
                item.duration,
                item.group
            );
            k++;
        }
        return musicArray;
    }

    getMusicById(id) {
        const items = this.readItems();
        for (const item of items['items']) {
            if (item.id == id) {
                return new Music(item.id, item.name, item.duration, item.group);
            }
        }
        return null;
    }

    addMusic(music) {
        let items = this.readItems();
        music.id = this.nextId;
        this.incrementNextId();
        items['items'].push(music);
        this.writeItems(items);
        console.log("new id: " + music.id);
        return;
    }

    updateMuse(NewMuse) {
        let counter = 0;
        const items = this.readItems();
        for (let i = 0; i < items['items'].length; i++) {
            if (NewMuse.id == items['items'][counter]['id']) {
                items['items'].splice(counter, 1, NewMuse);
                this.writeItems(items);
                return true;
            }
            counter++;
        }
        return false;
    }

    deleteMusic(id) {
        const items = this.readItems();
        let k = 0;
        for (const item of items['items']) {
            if (item.id == id) {
                items['items'].splice(k,1);
                this.writeItems(items);
                return;
            }
            k++;
        }
    }
};


module.exports = MusicRepository;