const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const as = require('../../controllers/Media_controller');
const { ayaya, rawr, getMediaById } = require('../../controllers/Media_controller');

/**
 * adds a new Image
 * @route POST /api/Media
 * @group Media - music operations
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploaded image
 * @returns {Media.model} 201 - added media object
 */
/**
 * returns a specific image
 * @route GET /api/Media/{id}
 * @group Media - media operations
 * @param {integer} id.path.required - id of the Media - eg: 1
 * @returns {Media.model} 200 - Music object
 * @returns {Error} 404 - Music not found
 */

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router
    .post('/', ayaya)
    .get('/:id', rawr);
    
module.exports = router;