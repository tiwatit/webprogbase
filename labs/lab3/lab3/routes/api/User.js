const express = require('express');
const router = express.Router();
/**
 * returns a specific user
 * @route GET /api/User/{id}
 * @group User - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {Error} 404 - User not found
 */
/**
 * TODO: Add some comment here
 * @route GET /api/User
 * @group User - user operations
 * @param {integer} page.query - page number
 * @param {integer} per_page.query - items per page
 * @returns {Array.<User>} User - a page with users
 */

/**
 * Adds a new user
 * @route POST /api/User
 * @group User - user operations
 * @param {User.model} id.body.required - new User object
 * @returns {User.model} 201 - added User object
 */
/**
 * edits a specific user
 * @route PUT /api/User
 * @group User - user operations
 * @param {User.model} id.body.required - new User object
 * @returns {User.model} 200 - changed User object
 */
/**
 * delets a specific user
 * @route DELETE /api/User/{id}
 * @group User - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - deleted User object
 * @returns {Error} 404 - User not found
 */


const bodyParser = require('body-parser');
const { getAllUsers, getUserById, addUser, updateUser, deleteUser} = require('../../controllers/User_controller');
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router
    .get('/', getAllUsers)
    .get('/:id', getUserById)
    .post('/', addUser)
    .put('/:id', updateUser)
    .delete('/:id', deleteUser);
module.exports = router; 
