// jsonStorage.js
class JsonStorage {

    // filePath - path to JSON file
    constructor(filePath) {
        this.filePath = filePath;
    }

    get nextId() {
        // TODO: get next entity id
        throw new Error("Not implemented.");
    }

    incrementNextId() {
        // TODO: increment next entity id 
        throw new Error("Not implemented.");
    }

    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray;
    }

    writeItems(items) {
        // TODO: write all items to JSON file
        throw new Error("Not implemented.");
    }
};

module.exports = JsonStorage;