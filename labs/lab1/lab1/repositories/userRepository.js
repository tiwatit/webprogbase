// repositories/user_repository.js
const User = require('./../model/user');
const jsonStorage = require('./../jsonStorage');
const readlineSync = require('readline-sync');
class UserRepository extends jsonStorage {
    getUsers() {
        const items = this.readItems();
        let userArray = [User];
        let k = 0;
        for (const item of items['items']) {
            userArray[k] = new User(
                item.id,
                item.login,
                item.fullname,
                item.role,
                item.registeredAt,
                item.avaUrl,
                item.isEnabled
            );
            k++;
        }
        return userArray;
    }

    getUserById(id) {
        const items = this.readItems();
        for (const item of items['items']) {
            if (item.id == id) {
                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
            }
        }
        return null;
    }

    addUser(user) {
        let items = this.readItems();
        user.id = this.nextId;
        this.incrementNextId();
        items['items'].push(user);
        this.writeItems(items);
        console.log("new id: " + user.id);
        return;
    }

    updateUser(user) {
        const items = this.readItems();
        for (let i = 0; i < items['items'].length; i++) {
            if (items['items'][i]['id'] == user.id) {
                items['items'].splice(i, 1, user);
                this.writeItems(items);
                return;
            }
        }
    }

    deleteUser(id) {
        const items = this.readItems();
        let k = 0;
        for (const item of items['items']) {
            if (item.id == id) {
                items['items'].splice(k, 1);
                this.writeItems(items);
                return;
            }
            k++;
        }
    }
};


module.exports = UserRepository;
