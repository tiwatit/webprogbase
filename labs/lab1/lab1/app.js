const JsonStorage = require('./jsonStorage');
const JsonRepository = require('./repositories/userRepository');
const MusicRepository = require('./repositories/musicRepository');
const userStorage = new JsonStorage('./data/users.json');
const userRepository = new JsonRepository('./data/users.json');
const musicRepository = new MusicRepository('./data/music.json');
const readlineSync = require('readline-sync');
const Music = require('./model/music');
const User = require('./model/user');
function isDate(str) {
    const parms = str.split(/[:.-/]/);
    const yyyy = parseInt(parms[0], 10);
    const mm = parseInt(parms[1], 10);
    const dd = parseInt(parms[2], 10);
    const h = parseInt(parms[3], 10);
    const m = parseInt(parms[4], 10);
    const date = new Date(yyyy, mm - 1, dd, h, m);
    return mm == (date.getMonth() + 1) &&
        dd == date.getDate() &&
        yyyy == date.getFullYear() &&
        h == date.getHours() &&
        m == date.getMinutes();
}
while (true) {
    const input = readlineSync.question('Enter a command: ');
    const cutInput = input.substr(0, input.lastIndexOf('/') + 1);
    if (input == 'get/music') {
        console.log(musicRepository.getMusic());
    }
    else if (cutInput == 'get/music/') {
        const id = input.substr(input.lastIndexOf('/') + 1);
        const muse = musicRepository.getMusicById(id);
        if (muse != null)
            console.log(muse.id + '/' + muse.name + '/' + muse.duration + '/' + muse.group);
        else
            console.error("Wrong id");
    }
    else if (cutInput == 'delete/music/') {
        const id = input.substr(input.lastIndexOf('/') + 1);
        const muse = musicRepository.getMusicById(id);
        if (muse != null) {
            musicRepository.deleteMusic(id);
        }
        else {
            console.error("Wrong id");
        }
    }
    else if (input == 'post/music') {
        let new_muse = new Music;
        new_muse.name = readlineSync.question('Enter a name: ');
        new_muse.duration = readlineSync.question('Enter a duration: ');
        new_muse.group = readlineSync.question('Enter a group: ');
        musicRepository.addMusic(new_muse);
    }
    else if (cutInput == 'update/music/') {
        const id = input.substr(input.lastIndexOf('/') + 1);
        const muse = musicRepository.getMusicById(id);
        if (muse != null) {
            let new_muse = new Music;
            new_muse.name = readlineSync.question('Enter a name: ');
            new_muse.duration = readlineSync.question('Enter a duration: ');
            new_muse.group = readlineSync.question('Enter a group: ');
            new_muse.id = muse.id;
            musicRepository.updateMusic(new_muse);
        }
    }
    else if (input == 'get/users') {
        console.log(userRepository.getUsers());
    }
    else if (input == 'post/user') {
        let new_user = new User;
        new_user.login = readlineSync.question('Enter a login: ');
        new_user.fullname = readlineSync.question('Enter a fullname: ');
        new_user.role = readlineSync.question('Enter a role: ');
        let input2 = readlineSync.question('Enter a reg.date: ');
        if (isDate(input2)) {
            new_user.registeredAt = input2;
        }
        else {
            console.log('Enter a correct date');
        }
        new_user.avaUrl = readlineSync.question('Enter a pfp Url: ');
        new_user.isEnabled = readlineSync.question('Enabled?: ');
        userRepository.addUser(new_user);
    }
    else if (cutInput == 'get/user/') {
        const id = input.substr(input.lastIndexOf('/') + 1);
        const user = userRepository.getUserById(id);
        console.log(user);
        if (user != null)
            console.log(user.id + '/' + user.login + '/' + user.fullname + '/' + user.role + '/' + user.registeredAt + '/' + user.avaUrl + '/' + user.isEnabled);
        else
            console.error("Wrong id");
    }
    else if (cutInput == 'update/user/') {
        const id = input.substr(input.lastIndexOf('/') + 1);
        const user = userRepository.getUserById(id);
        if (user != null) {
            let new_user = new User;
            new_user.login = readlineSync.question('Enter a login: ');
            new_user.fullname = readlineSync.question('Enter a fullname: ');
            new_user.role = readlineSync.question('Enter a role: ');
            let input2 = readlineSync.question('Enter a reg.date: ');
            if (isDate(input2)) {
                new_user.registeredAt = input2;
            }
            else {
                console.log('Enter a correct date');
            }
            new_user.avaUrl = readlineSync.question('Enter a pfp Url: ');
            new_user.isEnabled = readlineSync.question('Enabled?: ');
            new_user.id = user.id;
            userRepository.updateUser(new_user);
        }
    }
    else if (cutInput == 'delete/user/') {
        const id = input.substr(input.lastIndexOf('/') + 1);
        const user = userRepository.getUserById(id);
        if (user != null) {
            userRepository.deleteUser(id);
        }
        else {
            console.error("Wrong id");
        }
    }
};
setTimeout(function () {
    console.log('done');
    process.exit();
}, 2000); 


