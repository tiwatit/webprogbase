// jsonStorage.js
const fs = require('fs');
class JsonStorage {

    // filePath - path to JSON file
    constructor(filePath) {
        this.filePath = filePath;
    }

    get nextId() {
        const jsonArray = this.readItems();
        return jsonArray['items'][jsonArray['items'].length-1]['id']+1;
    }

    incrementNextId() {
        const jsonArray = this.readItems();
        jsonArray['nextId'] = jsonArray['nextId'] + 1;
        const json = JSON.stringify(jsonArray, null, 4);
        fs.writeFileSync(this.filePath, json, 'utf8');
    }
    
    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray;
    }


    writeItems(items) {
        const jsonArray = this.readItems();
        jsonArray['items'] = items['items']
        const json = JSON.stringify(jsonArray, null, 4);
        fs.writeFileSync(this.filePath, json, 'utf8');
    }
};

module.exports = JsonStorage;