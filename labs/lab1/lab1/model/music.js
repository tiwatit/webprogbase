class Music {
    constructor(id,name,duration,group) {
        this.id = id;
        this.name = name;
        this.duration = duration;
        this.group = group;
    }
};

module.exports = Music;