// models/user.js
class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login;  // string
        this.fullname = fullname;  // string
        this.role = role;   //number
        this.registeredAt = registeredAt;   //string
        this.avaUrl = avaUrl;   //string
        this.isEnabled = isEnabled; //boolean
    }
};

module.exports = User;